import os

import numpy as np
import torch
import cv2
from pytorch3d.io import load_objs_as_meshes
from pytorch3d.renderer import (
    BlendParams,
    FoVPerspectiveCameras,
    look_at_view_transform,
    MeshRasterizer,
    MeshRenderer,
    PointLights,
    AmbientLights,
    RasterizationSettings,
    SoftPhongShader,
    SoftSilhouetteShader,
)


# create the default data directory
current_dir = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(current_dir, "..", "data", "poterry_mesh")
IMAGE_SIZE = 800

def generate_poterry_renders(
    num_views: int = 40,
    data_dir: str = DATA_DIR, 
    azimuth_range: float = 180,
    elevation: int = 0,
    generate_silhouette: bool = False
):
    """
    Эта функция генерирует рендеры `num_views` сетки модели.
    Рендеры создаются с помощью точек обзора, отобранных с равномерно распределенных точек обзора.
    азимутальные интервалы. Угол возвышения поддерживается постоянным, так что камера находится в
    вертикальное положение совпадает с экватором.

    Аргументы:
        num_views: Количество сгенерированных рендеров.
        data_dir: Папка, содержащая файлы сетки.
        azimuth_range: количество градусов по обе стороны от начальной позиции для
        взятия проб.

    Возвращается:
        камеры: набор `num_views`, "FoVPerspectiveCameras", с которых
        обрабатываются изображения.
        изображения: Тензор формы "(num_views, высота, ширина, 3)", содержащий
        обработанные изображения.
        силуэты: Тензор формы (num_views, высота, ширина), содержащий
        отображаемые силуэты.
    """

      # Setup
    if torch.cuda.is_available():
        device = torch.device("cuda:0")
        torch.cuda.set_device(device)
    else:
        device = torch.device("cpu")
    # device = torch.device("cpu")

    # Load obj file
    obj_filename = os.path.join(data_dir, "Poterry.obj")

    mesh = load_objs_as_meshes([obj_filename], device=device)

    verts = mesh.verts_packed()
    N = verts.shape[0]
    center = verts.mean(0)
    scale = max((verts - center).abs().max(0)[0])
    mesh.offset_verts_(-(center.expand(N, 3)))
    mesh.scale_verts_((1.0 / float(scale)))


    elev = torch.linspace(elevation, elevation, num_views)
    azim = torch.linspace(-azimuth_range, azimuth_range, num_views) + 180.0

    lights = AmbientLights(device=device)


    R, T = look_at_view_transform(dist=2.7, elev=elev, azim=azim)
    cameras = FoVPerspectiveCameras(device=device, R=R, T=T)

    raster_settings = RasterizationSettings(
        image_size=IMAGE_SIZE, blur_radius=0.0, faces_per_pixel=1, bin_size=0, max_faces_per_bin=1000000
    )

    blend_params = BlendParams(sigma=1e-4, gamma=1e-4, background_color=(0.0, 0.0, 0.0))
    renderer = MeshRenderer(
        rasterizer=MeshRasterizer(cameras=cameras, raster_settings=raster_settings),
        shader=SoftPhongShader(
            device=device, cameras=cameras, lights=lights, blend_params=blend_params
        ),
    )

    meshes = mesh.extend(num_views)


    target_images = renderer(meshes, cameras=cameras, lights=lights)

    # Rasterization settings for silhouette rendering
    silhouette_binary = None
    if generate_silhouette:
        sigma = 1e-4
        raster_settings_silhouette = RasterizationSettings(
            image_size=IMAGE_SIZE, blur_radius=np.log(1.0 / 1e-4 - 1.0) * sigma, faces_per_pixel=50, bin_size=0
        )

        # Silhouette renderer
        renderer_silhouette = MeshRenderer(
            rasterizer=MeshRasterizer(
                cameras=cameras, raster_settings=raster_settings_silhouette
            ),
            shader=SoftSilhouetteShader(),
        )

        # Render silhouette images.  The 3rd channel of the rendering output is
        # the alpha/silhouette channel
        silhouette_images = renderer_silhouette(meshes, cameras=cameras, lights=lights)

        # binary silhouettes
        silhouette_binary = (silhouette_images[..., 3] > 1e-4).float()

    return cameras, target_images[..., :3], silhouette_binary



target_cameras, target_images, target_silhouettes = generate_poterry_renders(num_views=20, 
                                                                            azimuth_range=180,
                                                                            elevation=55)

clamp_and_detach = lambda x: x.clamp(0.0, 1.0).cpu().detach().numpy()

i = 40
for target_image in target_images:
    img = clamp_and_detach(target_image)*255
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    cv2.imwrite(img=img, filename=f'image{i:02d}.png')
    i += 1